<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $firstnama = $request['firstname'];
        $lastnama = $request['lastname'];
        $biodata = $request['bio'];
        return view('halaman.welcome', compact('firstnama','lastnama','biodata'));
    }
}
